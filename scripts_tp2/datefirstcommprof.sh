
#!/bin/bash

#on utilise le timestamp afin de trouver la date par rapport au 1er janvier 1970

tstamp=$(cat $2 | awk '{print $2}' | grep $1 | cut -d ':' -f1 | head -1)

#cat va permettre d'ouvrir le fichier donné en paramètre 
#awk '{print $2}' va afficher la deuxieme colonne de la sortie uniquement 
#grep $1 va permettre de garder le nombre de lignes définies dans la commande en paramètres  #cut -d ':' -f1 va permettre de couper la chaine de caractère où on a un ":" et prendre uniqement ce qui se trouve à gauche des ":"
#head -1 va permettre d'avoir la première commande tapée

#maintenant on utilise la commande date afin de trouver la date de la première commande lancée

date -d @$tstamp

#l'option -d avec @ va permettre de convertir le timestamp en une date que l'on peut comprendre
