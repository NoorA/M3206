#!/bin/bash

#On definit la commande pour lister le top des commandes 

cat my_history | awk '{print $2}' | cut -d ';' -f2 | sort | uniq -c | sort -nr | head -$1 

# awk '{print $2}' va afficher la deuxieme colonne de la sortie uniquement
#le cut -d et le -f2 vont respectivement couper en fonction de la chaine de caratère et garder ce qui est après cette chaine 
#sort va trier en ordre alphabetique
#uniq -c va compter le nombre d'occurence
#sort -nr va permettre de mettre dans l'ordre numérique croissant puis puis va retourner l'ordre décroissant
#head -$1 va permettre de faire afficher uniquement les x premières lignes données en paramètres 
