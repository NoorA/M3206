#!/bin/bash

#On commence par donner la date 

date=$(date +%Y_%m_%d_%H_%M)

#On définit le repertoire

repertoire=$(basename $1)

#On archive le fichier

tar -cvf $date_$repertoire.tar $1

#On compresse le fichier

gzip $date_$repertoire.tar

#On le déplace dans le repertoire de backup

mv $date_$repertoire.tar.gz /tmp/backup
